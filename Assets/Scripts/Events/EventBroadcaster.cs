﻿using System;
using System.Collections.Generic;

public class EventBroadcaster
{
    private static Dictionary<IGameEvent, List<Delegate>> m_EventDictionary = new Dictionary<IGameEvent, List<Delegate>>();

    #region Internal
    private static void RegisterInternal(IGameEvent gameEvent, Delegate listener)
    {
        List<Delegate> eventDelegates = null;

        if (m_EventDictionary.TryGetValue(gameEvent, out eventDelegates))
        {
            eventDelegates.Add(listener);
        }
        else
        {
            eventDelegates = new List<Delegate>
            {
                listener
            };

            m_EventDictionary.Add(gameEvent, eventDelegates);
        }
    }

    private static void UnregisterInternal(IGameEvent gameEvent, Delegate listener)
    {
        List<Delegate> eventDelegates = null;
        if (m_EventDictionary.TryGetValue(gameEvent, out eventDelegates))
        {
            eventDelegates.Remove(listener);
        }
    }

    private static void BroadcastInternal(IGameEvent gameEvent, params object[] paramObjs)
    {
        List<Delegate> eventDelegates = null;
        if (m_EventDictionary.TryGetValue(gameEvent, out eventDelegates))
        {
            eventDelegates.ForEach(x => x.DynamicInvoke(paramObjs));
        }
    }
    #endregion

    #region Register implementation
    public static void Register(GameEvent gameEvent, Action listener)
    {
        RegisterInternal(gameEvent, listener);
    }

    public static void Register<T>(GameEvent<T> gameEvent, Action<T> listener)
    {
        RegisterInternal(gameEvent, listener);
    }

    public static void Register<T, U>(GameEvent<T, U> gameEvent, Action<T, U> listener)
    {
        RegisterInternal(gameEvent, listener);
    }

    public static void Register<T, U, V>(GameEvent<T, U, V> gameEvent, Action<T, U, V> listener)
    {
        RegisterInternal(gameEvent, listener);
    }

    public static void Register<T, U, V, W>(GameEvent<T, U, V, W> gameEvent, Action<T, U, V, W> listener)
    {
        RegisterInternal(gameEvent, listener);
    }
    #endregion

    #region Broadcast implementation
    public static void Broadcast(GameEvent gameEvent)
    {
        BroadcastInternal(gameEvent);
    }

    public static void Broadcast<T>(GameEvent<T> gameEvent, T paramT)
    {
        BroadcastInternal(gameEvent, paramT);
    }

    public static void Broadcast<T, U>(GameEvent<T, U> gameEvent, T paramT, U paramU)
    {
        BroadcastInternal(gameEvent, paramT, paramU);
    }

    public static void Broadcast<T, U, V>(GameEvent<T, U, V> gameEvent, T paramT, U paramU, V paramV)
    {
        BroadcastInternal(gameEvent, paramT, paramU, paramV);
    }

    public static void Broadcast<T, U, V, W>(GameEvent<T, U, V, W> gameEvent, T paramT, U paramU, V paramV, W paramW)
    {
        BroadcastInternal(gameEvent, paramT, paramU, paramV, paramW);
    }
    #endregion

    #region Unregister implementation
    public static void Unregister(GameEvent gameEvent, Action listener)
    {
        UnregisterInternal(gameEvent, listener);
    }

    public static void Unregister<T>(GameEvent<T> gameEvent, Action<T> listener)
    {
        UnregisterInternal(gameEvent, listener);
    }

    public static void Unregister<T, U>(GameEvent<T, U> gameEvent, Action<T, U> listener)
    {
        UnregisterInternal(gameEvent, listener);
    }

    public static void Unregister<T, U, V>(GameEvent<T, U, V> gameEvent, Action<T, U, V> listener)
    {
        UnregisterInternal(gameEvent, listener);
    }

    public static void Unregister<T, U, V, W>(GameEvent<T, U, V, W> gameEvent, Action<T, U, V, W> listener)
    {
        UnregisterInternal(gameEvent, listener);
    }
    #endregion
}