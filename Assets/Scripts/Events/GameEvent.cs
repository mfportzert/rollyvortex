﻿using System.Collections.Generic;
using System;

public interface IGameEvent { }

public class GameEvent : IGameEvent
{
    public void Register(Action listener)
    {
        EventBroadcaster.Register(this, listener);
    }

    public void Unregister(Action listener)
    {
        EventBroadcaster.Unregister(this, listener);
    }

    public void Broadcast()
    {
        EventBroadcaster.Broadcast(this);
    }
}

public class GameEvent<T> : IGameEvent
{
    public void Register(Action<T> listener)
    {
        EventBroadcaster.Register(this, listener);
    }

    public void Unregister(Action<T> listener)
    {
        EventBroadcaster.Unregister(this, listener);
    }

    public void Broadcast(T paramT)
    {
        EventBroadcaster.Broadcast(this, paramT);
    }
}

public class GameEvent<T,U> : IGameEvent
{
    public void Register(Action<T, U> listener)
    {
        EventBroadcaster.Register(this, listener);
    }

    public void Unregister(Action<T, U> listener)
    {
        EventBroadcaster.Unregister(this, listener);
    }

    public void Broadcast(T paramT, U paramU)
    {
        EventBroadcaster.Broadcast(this, paramT, paramU);
    }
}

public class GameEvent<T,U,V> : IGameEvent
{
    public void Register(Action<T, U, V> listener)
    {
        EventBroadcaster.Register(this, listener);
    }

    public void Unregister(Action<T, U, V> listener)
    {
        EventBroadcaster.Unregister(this, listener);
    }

    public void Broadcast(T paramT, U paramU, V paramV)
    {
        EventBroadcaster.Broadcast(this, paramT, paramU, paramV);
    }
}

public class GameEvent<T,U,V,W> : IGameEvent
{
    public void Register(Action<T, U, V, W> listener)
    {
        EventBroadcaster.Register(this, listener);
    }

    public void Unregister(Action<T, U, V, W> listener)
    {
        EventBroadcaster.Unregister(this, listener);
    }

    public void Broadcast(T paramT, U paramU, V paramV, W paramW)
    {
        EventBroadcaster.Broadcast(this, paramT, paramU, paramV, paramW);
    }
}
