﻿using System.Collections.Generic;
using UnityEngine;

public class Events
{
    public class Gameplay
    {
        public static GameEvent ObstaclePassed = new GameEvent();
        public static GameEvent<Vector3> ObstacleHit = new GameEvent<Vector3>(); // impactPoint

        public static GameEvent<int, int> ScoreUpdated = new GameEvent<int, int>(); // newScore, scoreDelta
        public static GameEvent<int> HighScoreUpdated = new GameEvent<int>(); // newHighScore

        public static GameEvent StartGame = new GameEvent();
        public static GameEvent GameOver = new GameEvent();
        public static GameEvent RetryGame = new GameEvent();

        public static GameEvent<ChallengeConfig> StartChallenge = new GameEvent<ChallengeConfig>();
    }

    public class UI
    {
        public static GameEvent<EScreenType> ScreenOpened = new GameEvent<EScreenType>(); // Screen to open
        public static GameEvent<EScreenType> ScreenClosed = new GameEvent<EScreenType>(); // Screen to close
    }

    public class Save
    {
        public static GameEvent<SaveState> SaveGame = new GameEvent<SaveState>(); // State to save
        public static GameEvent<SaveState> LoadGame = new GameEvent<SaveState>(); // State to load
    }

    public class Camera
    {
        public static GameEvent Flash = new GameEvent();
        public static GameEvent Shake = new GameEvent();
    }
}