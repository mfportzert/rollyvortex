﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MenuScreen : MonoBehaviour
{
    [SerializeField] private Button m_StartButton = null;
    [SerializeField] private GameObject m_HighScorePanel = null;
    [SerializeField] private TextMeshProUGUI m_HighScoreText = null;
    [SerializeField] private TextMeshProUGUI m_StartText = null;

    private void Awake()
    {
        m_StartButton.onClick.AddListener(OnStartButtonClicked);
    }

    private void OnDestroy()
    {
        m_StartButton.onClick.RemoveListener(OnStartButtonClicked);
    }

    private void Start()
    {
        int highscore = GameManager.Instance.ScoreController.PlayerHighScore;
        if (highscore > 0)
        {
            m_HighScorePanel.SetActive(true);
            m_HighScoreText.text = highscore.ToString();

            //m_StartText.gameObject.SetActive(false);
        }
        else
        {
            m_HighScorePanel.SetActive(false);
            //m_StartText.gameObject.SetActive(true);
        }
    }

    private void OnStartButtonClicked()
    {
        Events.Gameplay.StartGame.Broadcast();
    }
}
