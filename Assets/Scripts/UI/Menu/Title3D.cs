﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Title3D : MonoBehaviour
{
    private void Awake()
    {
        Events.UI.ScreenOpened.Register(OnScreenOpened);
        Events.UI.ScreenClosed.Register(OnScreenClosed);
    }

    private void OnDestroy()
    {
        Events.UI.ScreenOpened.Unregister(OnScreenOpened);
        Events.UI.ScreenClosed.Unregister(OnScreenClosed);
    }

    private void OnScreenOpened(EScreenType screenType)
    {
        if (screenType == EScreenType.Game)
        {
            gameObject.SetActive(false);
        }
    }

    private void OnScreenClosed(EScreenType screenType)
    {
    }
}
