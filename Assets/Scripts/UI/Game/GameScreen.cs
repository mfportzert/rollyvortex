﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameScreen : MonoBehaviour
{
    [SerializeField] private Animator m_GameScreenAnimator = null;
    [SerializeField] private Animator m_ScoreAnimator = null;
    [SerializeField] private TextMeshProUGUI m_ScoreText = null;
    [SerializeField] private RectTransform m_HighScorePanel = null;
    [SerializeField] private TextMeshProUGUI m_HighScoreText = null;
    [SerializeField] private Button m_RetryButton = null;
    [SerializeField] private MedalProgressDisplayer m_MedalProgressDisplayer = null;

    private void Awake()
    {
        Events.Gameplay.ScoreUpdated.Register(OnScoreUpdated);
        Events.Gameplay.HighScoreUpdated.Register(OnHighScoreUpdated);
        Events.Gameplay.GameOver.Register(OnGameOver);
        Events.Gameplay.RetryGame.Register(OnRetryGame);

        m_RetryButton.onClick.AddListener(OnRetryButtonClicked);
    }

    private void OnDestroy()
    {
        Events.Gameplay.ScoreUpdated.Unregister(OnScoreUpdated);
        Events.Gameplay.HighScoreUpdated.Unregister(OnHighScoreUpdated);
        Events.Gameplay.GameOver.Unregister(OnGameOver);
        Events.Gameplay.RetryGame.Unregister(OnRetryGame);

        m_RetryButton.onClick.RemoveListener(OnRetryButtonClicked);
    }

    private void Start()
    {
        m_GameScreenAnimator.SetTrigger(AnimParams.ANIM_TRIGGER_ScreenOpen);

        InitLayout();
    }

    private void InitLayout()
    {
        bool isFirstGame = GameManager.Instance.IsFirstGameEver();
        m_HighScorePanel.gameObject.SetActive(!isFirstGame);

        SetScoreText(GameManager.Instance.ScoreController.PlayerScore);
        SetHighScoreText(GameManager.Instance.ScoreController.PlayerHighScore);
        
        m_ScoreAnimator.SetBool(AnimParams.ANIM_BOOL_IsHighScoreMode, false);
    }

    private void OnScoreUpdated(int newScore, int scoreDelta)
    {
        SetScoreText(newScore);
        m_ScoreAnimator.SetTrigger(AnimParams.ANIM_TRIGGER_AddScore);
    }

    private void OnHighScoreUpdated(int newHighScore)
    {
        SetHighScoreText(newHighScore);

        if (!GameManager.Instance.IsFirstGameEver())
        {
            m_ScoreAnimator.SetBool(AnimParams.ANIM_BOOL_IsHighScoreMode, true);
        }
    }

    private void SetScoreText(int score)
    {
        m_ScoreText.text = score.ToString();
    }

    private void SetHighScoreText(int highScore)
    {
        m_HighScoreText.text = highScore.ToString();
    }

    private void OnGameOver()
    {
        DisplayResult();
    }

    private void OnRetryGame()
    {
        InitLayout();
    }

    private void OnRetryButtonClicked()
    {
        m_GameScreenAnimator.SetTrigger(AnimParams.ANIM_TRIGGER_ScreenReset);

        Events.Gameplay.RetryGame.Broadcast();
    }

    private void DisplayResult()
    {
        if (GameManager.Instance.ScoreController.HasNewHighScore())
        {
            m_GameScreenAnimator.SetTrigger(AnimParams.ANIM_TRIGGER_ScreenResultNewBest);
            m_MedalProgressDisplayer.Setup();
        }
        else
        {
            m_GameScreenAnimator.SetTrigger(AnimParams.ANIM_TRIGGER_ScreenResult);
        }
    }
}
