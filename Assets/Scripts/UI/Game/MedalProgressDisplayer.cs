﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MedalProgressDisplayer : MonoBehaviour
{
    [SerializeField] private Animator m_Animator = null;
    [SerializeField] private Slider m_MedalProgressSlider = null;
    [SerializeField] private Image m_MedalImage = null;
    [SerializeField] private TextMeshProUGUI m_StartMedalScoreText = null;
    [SerializeField] private TextMeshProUGUI m_TargetMedalScoreText = null;
    [SerializeField] private TextMeshProUGUI m_PlayerScoreText = null;
    [SerializeField] private TextMeshProUGUI m_MedalNameText = null;

    [Header("Slider config")]
    [SerializeField] private float m_UpdateDelay = 1f;
    [SerializeField] private float m_SliderSpeed = 5f;

    private float m_TargetSliderValue = 0f;
    private float m_RemainingUpdateDelay = 0f;
    private bool m_MedalUnlocked = false;

    public void Setup()
    {
        int newHighScore = GameManager.Instance.ScoreController.PlayerHighScore;
        int previousHighScore = GameManager.Instance.ScoreController.PreviousPlayerHighScore;

        MedalConfig previousMedalConfig = MedalsManager.Instance.GetMedalConfig(previousHighScore);
        MedalConfig nextMedalConfig = MedalsManager.Instance.GetNextMedalConfig(previousHighScore);

        if (nextMedalConfig == null)
        {
            // Already maxed out
            gameObject.SetActive(false);
            return;
        }

        //if (m_MedalUnlocked)
        //{
        //    m_Animator.SetTrigger(AnimParams.ANIM_TRIGGER_ResetUnlockMedal);
        //}

        m_MedalUnlocked = false;
        m_RemainingUpdateDelay = m_UpdateDelay;
        m_MedalNameText.gameObject.SetActive(false);
        m_MedalNameText.text = nextMedalConfig.Name;

        int previousUnlockScore = (previousMedalConfig != null) ? previousMedalConfig.UnlockScore : 0;

        m_PlayerScoreText.text = newHighScore.ToString();
        m_StartMedalScoreText.text = previousUnlockScore.ToString();
        m_TargetMedalScoreText.text = nextMedalConfig.UnlockScore.ToString();

        m_MedalImage.sprite = nextMedalConfig.Icon;

        m_TargetSliderValue = CalculateProgress(newHighScore, previousUnlockScore, nextMedalConfig.UnlockScore);
        m_MedalProgressSlider.value = CalculateProgress(previousHighScore, previousUnlockScore, nextMedalConfig.UnlockScore);
    }

    private float CalculateProgress(float score, float min, float max)
    {
        float progress = 1f;
        if (min > 0)
        {
            progress = (score / min) - 1f;
        }
        else
        {
            progress = score / max;
        }

        return progress;
    }

    private void Update()
    {
        if (m_RemainingUpdateDelay <= 0)
        {
            float increaseDelta = (m_TargetSliderValue - m_MedalProgressSlider.value) * Time.deltaTime * m_SliderSpeed;

            m_MedalProgressSlider.value += increaseDelta;

            if (m_TargetSliderValue >= 1f && m_MedalProgressSlider.value >= 0.95f)
            {
                UnlockMedal();
            }
        }
        else
        {
            m_RemainingUpdateDelay -= Time.deltaTime;
        }
    }

    private void UnlockMedal()
    {
        if (!m_MedalUnlocked)
        {
            m_MedalUnlocked = true;
            m_Animator.SetTrigger(AnimParams.ANIM_TRIGGER_UnlockMedal);

            m_MedalNameText.gameObject.SetActive(true);
        }
    }
}
