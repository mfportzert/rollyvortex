﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PointsEarnedCanvas : MonoBehaviour, IPooledCallbacks
{
    [SerializeField] private TextMeshProUGUI m_EarnedPointsText = null;
    [SerializeField] private float m_ReturnToPoolDelay = 3f;

    private float m_ReturnToPoolRemainingDelay = 0f;

    public void SetEarnedPoints(int score)
    {
        m_EarnedPointsText.text = "+ " + score;
    }

    private void Update()
    {
        m_ReturnToPoolRemainingDelay -= Time.deltaTime;

        if (m_ReturnToPoolRemainingDelay <= 0f)
        {
            Pools.Send(gameObject);
        }
    }

    public void OnEnterPool()
    {
    }

    public void OnExitPool()
    {
        m_ReturnToPoolRemainingDelay = m_ReturnToPoolDelay;
    }
}
