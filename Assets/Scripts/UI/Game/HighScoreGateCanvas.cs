﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HighScoreGateCanvas : MonoBehaviour
{
    [SerializeField] private Animator m_Animator = null;
    [SerializeField] private CanvasGroup m_CanvasGroup = null;
    [SerializeField] private float m_StartDisplayAtDistance = 10f;
    [SerializeField] private TextMeshProUGUI m_ScoreText = null;

    private bool m_Passed = false;
    private bool m_GateEnabled = false;

    private void Awake()
    {
        Events.Gameplay.StartGame.Register(OnStartGame);
        Events.Gameplay.RetryGame.Register(OnRetryGame);

        gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        Events.Gameplay.StartGame.Unregister(OnStartGame);
        Events.Gameplay.RetryGame.Unregister(OnRetryGame);
    }

    private void OnStartGame()
    {
        SetupHighScoreGate();
    }

    private void OnRetryGame()
    {
        SetupHighScoreGate();
    }

    private void SetupHighScoreGate()
    {
        if (GameManager.Instance.IsFirstGameEver())
        {
            m_GateEnabled = false;
            return;
        }

        m_GateEnabled = true;
        SetGatePassed(false);
        gameObject.SetActive(true);

        Vector3 position = transform.position;
        int playerHighScore = GameManager.Instance.ScoreController.PlayerHighScore;
        int scorePerObstacle = GameManager.Instance.GameConfig.ScorePerObstacle;
        int highScoreObstacleIndex = playerHighScore / scorePerObstacle;

        position.z = GameManager.Instance.ObstacleGenerator.GetObstacleZPositionByIndex(highScoreObstacleIndex);

        transform.position = position;

        m_ScoreText.text = playerHighScore.ToString();
    }

    private void Update()
    {
        if (m_GateEnabled && !m_Passed)
        {
            UpdateVisibility();
            CheckGatePassed();
        }
    }

    private void CheckGatePassed()
    {
        Vector3 playerPosition = GameManager.Instance.GetPlayerPosition();
        if (playerPosition.z > transform.position.z)
        {
            SetGatePassed(true);

            Events.Camera.Flash.Broadcast();
        }
    }

    private void SetGatePassed(bool passed)
    {
        m_Passed = passed;
        m_Animator.SetBool(AnimParams.ANIM_BOOL_GatePassed, passed);
    }

    private void UpdateVisibility()
    {
        float alpha = 0;

        Vector3 playerPosition = GameManager.Instance.GetPlayerPosition();
        if (playerPosition.z + m_StartDisplayAtDistance > transform.position.z)
        {
            float distanceToPlayer = transform.position.z - playerPosition.z;
            alpha = distanceToPlayer / m_StartDisplayAtDistance;
        }

        m_CanvasGroup.alpha = alpha;
    }
}
