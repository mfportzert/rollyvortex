﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChallengesSection : MonoBehaviour
{
    [SerializeField] private ChallengesSectionHeader m_SectionHeader = null;
    [SerializeField] private GridLayoutGroup m_ChallengesGrid = null;
    [SerializeField] private ChallengeItem m_ChallengeItemPrefab = null;

    public void Setup(ChallengesSectionConfig sectionConfig)
    {
        m_SectionHeader.SetSectionName(sectionConfig.SectionName);

        foreach (ChallengeConfig challengeConfig in sectionConfig.Challenges)
        {
            ChallengeItem itemInstance = GameObject.Instantiate(m_ChallengeItemPrefab);
            itemInstance.Setup(challengeConfig);

            itemInstance.transform.SetParent(m_ChallengesGrid.transform, false);
        }
    }
}
