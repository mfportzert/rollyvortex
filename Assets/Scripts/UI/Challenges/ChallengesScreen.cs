﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChallengesScreen : MonoBehaviour
{
    [SerializeField] private VerticalLayoutGroup m_SectionsLayoutGroup = null;
    [SerializeField] private ChallengesSection m_SectionPrefab = null;

    private void Awake()
    {
        FillChallengesGrids();
    }

    private void FillChallengesGrids()
    {
        List<ChallengesSectionConfig> sectionsConfig = ChallengesManager.Instance.ChallengesConfig.Sections;
        foreach (ChallengesSectionConfig sectionConfig in sectionsConfig)
        {
            ChallengesSection sectionInstance = GameObject.Instantiate(m_SectionPrefab);
            sectionInstance.Setup(sectionConfig);

            sectionInstance.transform.SetParent(m_SectionsLayoutGroup.transform, false);
        }
    }
}
