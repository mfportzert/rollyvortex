﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChallengesSectionHeader : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_SectionName = null;

    public void SetSectionName(string sectionName)
    {
        m_SectionName.text = sectionName;
    }
}
