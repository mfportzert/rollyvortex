﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChallengeItem : MonoBehaviour
{
    [SerializeField] private Image m_Icon = null;
    [SerializeField] private Image m_LockedIcon = null;
    [SerializeField] private TextMeshProUGUI m_ChallengeName = null;
    [SerializeField] private Button m_Button = null;

    private ChallengeConfig m_ChallengeConfig;

    private void Awake()
    {
        m_Button.onClick.AddListener(OnChallengeClicked);
    }

    private void OnDestroy()
    {
        m_Button.onClick.RemoveListener(OnChallengeClicked);
    }

    private void OnChallengeClicked()
    {
        Debug.LogError("Challenge click");

        Events.Gameplay.StartChallenge.Broadcast(m_ChallengeConfig);
    }

    public void Setup(ChallengeConfig challengeConfig)
    {
        m_ChallengeConfig = challengeConfig;

        //int playerHighScore = GameManager.Instance.ScoreController.PlayerHighScore;

        m_Icon.sprite = challengeConfig.Icon;
        m_ChallengeName.text = challengeConfig.Name;
        //m_UnlockScore.text = playerHighScore + " / " + medalConfig.UnlockScore.ToString();

        //bool unlocked = playerHighScore > medalConfig.UnlockScore;
        //if (unlocked)
        //{
        //    m_LockedIcon.gameObject.SetActive(false);
        //    m_MedalName.gameObject.SetActive(true);
        //    m_UnlockScore.gameObject.SetActive(false);
        //}
        //else
        //{
        //    m_LockedIcon.gameObject.SetActive(true);
        //    m_MedalName.gameObject.SetActive(false);
        //    m_UnlockScore.gameObject.SetActive(true);
        //}
    }
}
