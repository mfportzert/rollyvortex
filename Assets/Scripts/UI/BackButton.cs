﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class BackButton : MonoBehaviour
{
    [SerializeField] private EScreenType m_ScreenToOpen = EScreenType.None;

    private Button m_Button;

    private void Awake()
    {
        m_Button = GetComponent<Button>();
        m_Button.onClick.AddListener(OnButtonClicked);
    }

    void Update()
    {
#if UNITY_ANDROID
        if (Input.GetKeyDown(KeyCode.Escape)) 
        {
            OnBackButtonPressed();
        }
#endif
    }

    private void OnBackButtonPressed()
    {
        TryOpenNextScreen();
    }

    private void OnButtonClicked()
    {
        TryOpenNextScreen();
    }

    private void TryOpenNextScreen()
    {
        if (m_ScreenToOpen != EScreenType.None)
        {
            ScreenManager.Instance.OpenScreen(m_ScreenToOpen);
        }
    }
}
