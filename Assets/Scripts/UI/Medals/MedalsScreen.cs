﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MedalsScreen : MonoBehaviour
{
    [SerializeField] private GridLayoutGroup m_GridLayoutGroup = null;
    [SerializeField] private MedalItem m_MedalItemPrefab = null;

    private List<MedalItem> m_MedalItems = new List<MedalItem>();

    private void Awake()
    {
        FillMedalsGrid();
    }

    private void OnEnable()
    {
        Refresh();
    }

    private void FillMedalsGrid()
    {
        List<MedalConfig> medalsConfig = MedalsManager.Instance.MedalsConfig.Medals;
        foreach (MedalConfig medalConfig in medalsConfig)
        {
            MedalItem itemInstance = GameObject.Instantiate(m_MedalItemPrefab);
            itemInstance.transform.SetParent(m_GridLayoutGroup.transform, false);
            itemInstance.Setup(medalConfig);

            m_MedalItems.Add(itemInstance);
        }
    }

    private void Refresh()
    {
        foreach (MedalItem item in m_MedalItems)
        {
            item.Refresh();
        }
    }
}
