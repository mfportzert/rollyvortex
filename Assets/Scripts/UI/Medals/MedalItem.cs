﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MedalItem : MonoBehaviour
{
    [SerializeField] private Image m_Icon = null;
    [SerializeField] private TextMeshProUGUI m_MedalName = null;
    [SerializeField] private TextMeshProUGUI m_UnlockScore = null;
    [SerializeField] private Image m_LockedIcon = null;

    private MedalConfig m_MedalConfig;

    public void Setup(MedalConfig medalConfig)
    {
        m_MedalConfig = medalConfig;

        m_Icon.sprite = m_MedalConfig.Icon;
        m_MedalName.text = m_MedalConfig.Name;

        Refresh();
    }

    public void Refresh()
    {
        int playerHighScore = GameManager.Instance.ScoreController.PlayerHighScore;

        m_UnlockScore.text = playerHighScore + " / " + m_MedalConfig.UnlockScore.ToString();

        bool unlocked = playerHighScore > m_MedalConfig.UnlockScore;
        if (unlocked)
        {
            m_LockedIcon.gameObject.SetActive(false);
            m_MedalName.gameObject.SetActive(true);
            m_UnlockScore.gameObject.SetActive(false);
        }
        else
        {
            m_LockedIcon.gameObject.SetActive(true);
            m_MedalName.gameObject.SetActive(false);
            m_UnlockScore.gameObject.SetActive(true);
        }
    }
}
