﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class OpenScreenOnButtonClick : MonoBehaviour
{
    [SerializeField] private EScreenType m_ScreenToOpen = EScreenType.None;

    private Button m_Button;

    private void Awake()
    {
        m_Button = GetComponent<Button>();
        m_Button.onClick.AddListener(OnButtonClicked);
    }

    private void OnButtonClicked()
    {
        TryOpenScreen();
    }

    private void TryOpenScreen()
    {
        if (m_ScreenToOpen != EScreenType.None)
        {
            ScreenManager.Instance.OpenScreen(m_ScreenToOpen);
        }
    }
}
