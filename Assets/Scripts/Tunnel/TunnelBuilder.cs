﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TunnelBuilder : MonoBehaviour
{
    [SerializeField] private Pipe m_PipePrefab = null;
    [SerializeField] private int m_InitialPipesCount = 10;

    [SerializeField] private float m_SwapPipesPlayerDistance = 15f;

    private List<Pipe> m_TunnelPipes = new List<Pipe>();

    private void Awake()
    {
        Setup();
    }
    
    private void Setup()
    {
        CreatePipeAmount(m_InitialPipesCount);
    }

    private void CreatePipeAmount(int amount)
    {
        for (int i = 0; i < amount; ++i)
        {
            Pipe pipeInstance = GetPipeInstance();
            if (pipeInstance == null)
            {
                Debug.LogError("Tunnel Broken: Unable to get a proper Pipe instance");
                continue;
            }

            if (m_TunnelPipes.Count > 0)
            {
                pipeInstance.SnapTo(m_TunnelPipes[m_TunnelPipes.Count - 1]);
            }
            else
            {
                pipeInstance.transform.position = Vector3.zero;
            }

            m_TunnelPipes.Add(pipeInstance);
        }
    }

    private Pipe GetPipeInstance()
    {
        return Pools.GetFromPool(m_PipePrefab);
    }

    private void SwapPipes()
    {
        if (m_TunnelPipes.Count > 0)
        {
            Pipe pipe = m_TunnelPipes[0];
            pipe.SnapTo(m_TunnelPipes[m_TunnelPipes.Count - 1]);

            m_TunnelPipes.RemoveAt(0);
            m_TunnelPipes.Add(pipe);
        }
    }

    public void Update()
    {
        CheckSwapPipes();
    }

    private void CheckSwapPipes()
    {
        if (m_TunnelPipes.Count > 0)
        {
            Vector3 playerPosition = GameManager.Instance.GetPlayerPosition();
            float distanceFromFirstPipe = Vector3.Distance(playerPosition, m_TunnelPipes[0].transform.position);

            if (distanceFromFirstPipe > m_SwapPipesPlayerDistance)
            {
                SwapPipes();
            }
        }
    }
    
    public void ResetAll()
    {
        foreach (Pipe pipe in m_TunnelPipes)
        {
            Pools.Send(pipe.gameObject);
        }

        m_TunnelPipes.Clear();

        Setup();
    }
}
