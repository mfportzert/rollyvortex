﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pipe : MonoBehaviour
{
    [SerializeField] private float m_Length = 10f;

    public float Length
    {
        get { return m_Length; }
    }

    public void SnapTo(Pipe pipe)
    {
        Vector3 snapPosition = pipe.transform.position;
        snapPosition.z += pipe.Length;

        transform.position = snapPosition;
    }
}
