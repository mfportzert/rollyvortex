﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ObstacleGenerator : MonoBehaviour
{
    private class ObstacleGroup
    {
        public ObstacleGroupConfig Config { get; private set; }
        public Color GroupColor { get; private set; }
        public int GroupAmount { get; private set; }
        public int CurrentObstacleIndex { get; private set; }

        public void Init(ObstacleGroupConfig config, Color groupColor)
        {
            Config = config;
            GroupAmount = UnityEngine.Random.Range(config.MinGroupAmount, config.MaxGroupAmount + 1);
            GroupColor = groupColor;
            CurrentObstacleIndex = 0;
        }

        public void IncrementObstacleIndex()
        {
            CurrentObstacleIndex += 1;
        }

        public bool IsFinished()
        {
            return CurrentObstacleIndex >= GroupAmount;
        }
    }

    [SerializeField] private float m_FirstObstacleDistance = 20f;
    [SerializeField] private float m_BufferDistanceBeforeObstacles = 30f;

    [Header("Destroy")]
    [SerializeField] private float m_DestroyObstaclesPassedDistance = 10f;

    private float m_LastObstacleDistance = 0f;
    private bool m_GenerationEnabled = false;
    private List<Obstacle> m_GeneratedObstacles = new List<Obstacle>();

    private ObstacleGroup m_CurrentObstacleGroup = new ObstacleGroup();

    public void StartGeneration()
    {
        m_GenerationEnabled = true;

        GenerateInitialObstacles();
    }

    public void StopGeneration()
    {
        m_GenerationEnabled = false;
    }

    private void GenerateInitialObstacles()
    {
        int maxIterations = 10;

        bool success = false;
        int iterationCount = 0;
        do
        {
            success = TryGenerateNextObstacle();
            iterationCount++;
        }
        while (success && iterationCount < maxIterations);
    }

    private bool TryGenerateNextObstacle()
    {
        float traveledDistance = GameManager.Instance.GetTraveledDistance();

        float nextObstacleDistance = m_LastObstacleDistance;
        if (Mathf.Approximately(nextObstacleDistance, 0f))
        {
            nextObstacleDistance = m_FirstObstacleDistance;
        }
        else
        {
            nextObstacleDistance += GameManager.Instance.GameConfig.ObstaclesDistance;
        }

        if (traveledDistance + m_BufferDistanceBeforeObstacles > nextObstacleDistance)
        {
            Obstacle generatedObstacle = SpawnObstacle(nextObstacleDistance);
            m_GeneratedObstacles.Add(generatedObstacle);
            m_LastObstacleDistance = nextObstacleDistance;
            return true;
        }
        return false;
    }

    private Obstacle SpawnObstacle(float distance)
    {
        Vector3 obstaclePosition = new Vector3(0, 0, distance);

        Obstacle obstaclePrefab = GetObstaclePrefabFromCurrentObstacleGroup();
        Obstacle obstacleInstance = Pools.GetFromPool(obstaclePrefab);
        obstacleInstance.transform.position = obstaclePosition;
        obstacleInstance.SetColor(m_CurrentObstacleGroup.GroupColor);

        Obstacle lastGeneratedObstacle = (m_GeneratedObstacles.Count > 0) ? m_GeneratedObstacles[m_GeneratedObstacles.Count - 1] : null;
        obstacleInstance.RandomizeAngle(lastGeneratedObstacle);

        return obstacleInstance;
    }

    private Obstacle GetObstaclePrefabFromCurrentObstacleGroup()
    {
        if (m_CurrentObstacleGroup == null || m_CurrentObstacleGroup.IsFinished())
        {
            ObstacleGroupConfig config = GameManager.Instance.GameConfig.ObstaclesConfig.GetRandomObstacleGroup();
            Color groupColor = GameManager.Instance.GameConfig.ThemeConfig.GetRandomObstacleColor();

            m_CurrentObstacleGroup.Init(config, groupColor);
        }

        m_CurrentObstacleGroup.IncrementObstacleIndex();
        return m_CurrentObstacleGroup.Config.ObstaclePrefab;
    }

    private void Update()
    {
        if (m_GenerationEnabled)
        {
            TryGenerateNextObstacle();
        }

        CheckObstaclesToDestroy();
    }

    private void CheckObstaclesToDestroy()
    {
        Vector3 playerPosition = GameManager.Instance.GetPlayerPosition();

        for (int i = m_GeneratedObstacles.Count - 1; i >= 0; --i)
        {
            float distanceToObstacle = playerPosition.z - m_GeneratedObstacles[i].transform.position.z;
            if (distanceToObstacle > m_DestroyObstaclesPassedDistance)
            {
                Pools.Send(m_GeneratedObstacles[i].gameObject);
                m_GeneratedObstacles.RemoveAt(i);
            }
        }
    }
    
    public void ResetAll()
    {
        m_LastObstacleDistance = 0f;

        foreach (Obstacle obstacle in m_GeneratedObstacles)
        {
            Pools.Send(obstacle.gameObject);
        }

        m_GeneratedObstacles.Clear();
    }

    public float GetObstacleZPositionByIndex(int obstacleIndex)
    {
        return m_FirstObstacleDistance + (obstacleIndex * GameManager.Instance.GameConfig.ObstaclesDistance);
    }
}
