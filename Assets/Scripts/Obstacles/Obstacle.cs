﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour, IPooledCallbacks
{
    private const float VISIBILITY_DISTANCE = 15f;

    [SerializeField] private Renderer m_Renderer = null;

    [Header("Angle Config")]
    [SerializeField] private float m_DefaultAngle = 180f;
    [SerializeField] private float m_MinAngle = -90f;
    [SerializeField] private float m_MaxAngle = 90f;
    [SerializeField] private float m_NextObstacleMaxAngleDelta = 90f;

    [Header("Behaviour")]
    [Range(0f,1f)]
    [SerializeField] private float m_ChanceToAutoRotate = 0f;
    [SerializeField] private float m_AutoRotateSpeed = 1f;

    private bool m_Passed = false;
    private float m_Angle = 0f;

    private bool m_AutoRotate = false;
    private int m_AutoRotateDirection = 1;

    private Quaternion m_InitialLocalRotation = Quaternion.identity;

    public float Angle
    {
        get { return m_Angle; }
    }

    public float MinAngle
    {
        get { return m_MinAngle; }
    }

    public float MaxAngle
    {
        get { return m_MaxAngle; }
    }

    public float NextObstacleMaxAngleDelta
    {
        get { return m_NextObstacleMaxAngleDelta; }
    }

    private void Awake()
    {
        m_InitialLocalRotation = transform.localRotation;
    }

    private void Update()
    {
        CheckPassed();

        UpdateVisibility();

        if (m_AutoRotate)
        {
            ApplyAutoRotate();
        }
    }

    private void CheckPassed()
    {
        if (!m_Passed)
        {
            Vector3 playerPosition = GameManager.Instance.GetPlayerPosition();
            if (playerPosition.z - 0.5f > transform.position.z)
            {
                m_Passed = true;

                Events.Gameplay.ObstaclePassed.Broadcast();
            }
        }
    }

    public void RandomizeAngle(Obstacle previousObstacle = null)
    {
        float generatedAngle = m_DefaultAngle;

        if (previousObstacle != null)
        {
            generatedAngle = Random.Range(MinAngle, MaxAngle);
            float previousAngle = previousObstacle.Angle;
            
            float deltaAngle = Mathf.DeltaAngle(previousAngle, generatedAngle);
            if (deltaAngle > previousObstacle.NextObstacleMaxAngleDelta)
            {
                generatedAngle = previousAngle + previousObstacle.NextObstacleMaxAngleDelta;
            }
        }

        m_Angle = generatedAngle;
        transform.Rotate(Vector3.forward, m_Angle);
    }

    private void UpdateVisibility()
    {
        Vector3 playerPosition = GameManager.Instance.GetPlayerPosition();
        if (transform.position.z - playerPosition.z > VISIBILITY_DISTANCE)
        {
            m_Renderer.enabled = false;
        }
        else
        {
            m_Renderer.enabled = true;
        }
    }

    private void RollAutoRotate()
    {
        m_AutoRotate = Random.value <= m_ChanceToAutoRotate;
        if (m_AutoRotate)
        {
            m_AutoRotateDirection = Random.Range(0, 2) == 0 ? -1 : 1;
        }
    }

    private void ApplyAutoRotate()
    {
        transform.Rotate(Vector3.forward, m_AutoRotateDirection * m_AutoRotateSpeed * Time.deltaTime);
    }

    public void SetColor(Color color)
    {
        m_Renderer.material.SetColor("_Color", color);
    }

    public void OnEnterPool()
    {
    }

    public void OnExitPool()
    {
        m_Passed = false;
        transform.localRotation = m_InitialLocalRotation;

        RollAutoRotate();
    }
}
