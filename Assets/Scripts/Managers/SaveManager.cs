﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SaveState
{
    public int HighScore = 0;
    public int PreviousHighScore = 0;
    public int NbGamesPlayed = 0;
}

public class SaveManager : Singleton<SaveManager>
{
    private string PREF_SAVE_KEY = "save";

    protected override void Awake()
    {
        base.Awake();
    }

    public void SaveGame()
    {
        SaveState saveState = new SaveState();

        Events.Save.SaveGame.Broadcast(saveState);

        string jsonSave = JsonUtility.ToJson(saveState);
        PlayerPrefs.SetString(PREF_SAVE_KEY, jsonSave);
    }

    public void LoadGame()
    {
        SaveState stateToLoad = null;

        if (PlayerPrefs.HasKey(PREF_SAVE_KEY))
        {
            string jsonData = PlayerPrefs.GetString(PREF_SAVE_KEY);
            stateToLoad = JsonUtility.FromJson<SaveState>(jsonData);
        }
        else
        {
            stateToLoad = new SaveState();
        }

        Events.Save.LoadGame.Broadcast(stateToLoad);
    }
}
