﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ChallengesManager : Singleton<ChallengesManager>
{
    [SerializeField] private ChallengesConfigSO m_ChallengesConfig = null;

    public ChallengesConfigSO ChallengesConfig
    {
        get { return m_ChallengesConfig; }
    }
}
