﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : Singleton<GameManager>
{
    [SerializeField] private Player m_Player = null;
    [SerializeField] private ScoreController m_ScoreController = null;
    [SerializeField] private TunnelBuilder m_TunnelBuilder = null;
    [SerializeField] private ObstacleGenerator m_ObstacleGenerator = null;
    [SerializeField] private GameConfigSO m_GameConfig = null;

    // TODO: Create Player stats data
    private int m_NbGamesPlayed = 0;

    private GameConfigSO m_CurrentGameConfig;

    public GameConfigSO GameConfig
    {
        get { return m_CurrentGameConfig; }
    }

    public ScoreController ScoreController
    {
        get { return m_ScoreController; }
    }

    public ObstacleGenerator ObstacleGenerator
    {
        get { return m_ObstacleGenerator; }
    }

    public Player Player
    {
        get { return m_Player; }
    }

    public int NbGamesPlayed
    {
        get { return m_NbGamesPlayed; }
    }

    protected override void Awake()
    {
        base.Awake();

        m_CurrentGameConfig = m_GameConfig;
        m_ScoreController.ResetScore();

        Events.Gameplay.ObstacleHit.Register(OnObstacleHit);
        Events.Gameplay.RetryGame.Register(OnRetryGame);
        Events.Gameplay.StartGame.Register(OnStartGame);
        Events.Gameplay.StartChallenge.Register(OnStartChallenge);
        Events.Save.SaveGame.Register(OnSaveGame);
        Events.Save.LoadGame.Register(OnLoadGame);
    }

    private void OnDestroy()
    {
        Events.Gameplay.ObstacleHit.Unregister(OnObstacleHit);
        Events.Gameplay.RetryGame.Unregister(OnRetryGame);
        Events.Gameplay.StartGame.Unregister(OnStartGame);
        Events.Gameplay.StartChallenge.Unregister(OnStartChallenge);
        Events.Save.SaveGame.Unregister(OnSaveGame);
        Events.Save.LoadGame.Unregister(OnLoadGame);
    }

    private void Start()
    {
        SaveManager.Instance.LoadGame();

        ScreenManager.Instance.OpenScreen(EScreenType.Menu);
    }

    public Vector3 GetPlayerPosition()
    {
        return (m_Player != null) ? m_Player.transform.position : Vector3.zero;
    }

    public float GetTraveledDistance()
    {
        Vector3 playerInitialPosition = (m_Player != null) ? m_Player.InitialPosition : Vector3.zero;
        return Vector3.Distance(GetPlayerPosition(), playerInitialPosition);
    }

    private void OnObstacleHit(Vector3 impactPoint)
    {
        m_Player.Die();
        m_ObstacleGenerator.StopGeneration();

        Events.Camera.Shake.Broadcast();
        Events.Gameplay.GameOver.Broadcast();

        m_NbGamesPlayed++;
        SaveManager.Instance.SaveGame();
    }
    
    private void OnStartGame()
    {
        ScreenManager.Instance.OpenScreen(EScreenType.Game);

        m_CurrentGameConfig = m_GameConfig;
        InitGame();
    }

    private void OnStartChallenge(ChallengeConfig challengeConfig)
    {
        ScreenManager.Instance.OpenScreen(EScreenType.Game);

        m_CurrentGameConfig = challengeConfig.GameConfig;
        InitGame();
    }

    private void OnRetryGame()
    {
        InitGame();
    }

    private void InitGame()
    {
        m_ScoreController.ResetScore();

        m_Player.ResetAll();
        m_TunnelBuilder.ResetAll();
        m_ObstacleGenerator.ResetAll();
        m_ObstacleGenerator.StartGeneration();
    }

    public bool IsFirstGameEver()
    {
        return m_NbGamesPlayed == 0;
    }

    private void OnSaveGame(SaveState saveState)
    {
        saveState.NbGamesPlayed = m_NbGamesPlayed;
    }

    private void OnLoadGame(SaveState saveState)
    {
        m_NbGamesPlayed = saveState.NbGamesPlayed;
    }
}
