﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum EScreenType
{
    None = 0,
    Menu = 1,
    Game = 2,
    Medals = 3,
    Challenges = 4,
}

public class ScreenManager : Singleton<ScreenManager>
{
    [Serializable]
    private class ScreenInfo
    {
        public EScreenType ScreenType = EScreenType.None;
        public GameObject ScreenPrefab = null;
    }

    [SerializeField] private List<ScreenInfo> m_ScreensInfo = null;

    private Dictionary<EScreenType, GameObject> m_ScreensDictionary = new Dictionary<EScreenType, GameObject>();
    private EScreenType m_CurrentScreenType = EScreenType.None;

    protected override void Awake()
    {
        base.Awake();

        PreloadScreens();
    }

    private void PreloadScreens()
    {
        foreach (ScreenInfo screenInfo in m_ScreensInfo)
        {
            if (screenInfo.ScreenPrefab != null && screenInfo.ScreenType != EScreenType.None)
            {
                if (!m_ScreensDictionary.ContainsKey(screenInfo.ScreenType))
                {
                    GameObject m_ScreenInstance = GameObject.Instantiate(screenInfo.ScreenPrefab, transform);
                    m_ScreenInstance.gameObject.SetActive(false);

                    m_ScreensDictionary.Add(screenInfo.ScreenType, m_ScreenInstance);
                }
            }
        }
    }

    public void OpenScreen(EScreenType screenType)
    {
        if (m_CurrentScreenType != EScreenType.None)
        {
            EScreenType screenTypeToClose = m_CurrentScreenType;
            SetScreenActive(m_CurrentScreenType, false);
            Events.UI.ScreenClosed.Broadcast(screenTypeToClose);
        }

        SetScreenActive(screenType, true);
        m_CurrentScreenType = screenType;
        Events.UI.ScreenOpened.Broadcast(screenType);
    }

    private void SetScreenActive(EScreenType screenType, bool active)
    {
        if (m_ScreensDictionary.ContainsKey(screenType))
        {
            m_ScreensDictionary[screenType].gameObject.SetActive(active);
        }
    }
}
