﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MedalsManager : Singleton<MedalsManager>
{
    [SerializeField] private MedalsConfigSO m_MedalConfigs = null;

    public MedalsConfigSO MedalsConfig
    {
        get { return m_MedalConfigs; }
    }

    public MedalConfig GetMedalConfig(int score)
    {
        MedalConfig highestMedalConfig = null;
        foreach (MedalConfig medalConfig in m_MedalConfigs.Medals)
        {
            if (score >= medalConfig.UnlockScore)
            {
                if (highestMedalConfig == null || medalConfig.UnlockScore > highestMedalConfig.UnlockScore)
                {
                    highestMedalConfig = medalConfig;
                }
            }
        }

        return highestMedalConfig;
    }

    public MedalConfig GetNextMedalConfig(int score)
    {
        MedalConfig nextMedalConfig = null;
        foreach (MedalConfig medalConfig in m_MedalConfigs.Medals)
        {
            if (score < medalConfig.UnlockScore)
            {
                if (nextMedalConfig == null || medalConfig.UnlockScore < nextMedalConfig.UnlockScore)
                {
                    nextMedalConfig = medalConfig;
                }
            }
        }

        return nextMedalConfig;
    }
}
