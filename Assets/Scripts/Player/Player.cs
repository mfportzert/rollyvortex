﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private Rigidbody m_Rigidbody = null;
    [SerializeField] private GameObject m_Ball = null;
    [SerializeField] private RunnerMovement m_RunnerMovement = null;
    [SerializeField] private PlayerMovementInput m_PlayerInputMovement = null;

    private Vector3 m_InitialPosition = Vector3.zero;

    public Rigidbody Rigidbody
    {
        get { return m_Rigidbody; }
    }

    public GameObject Ball
    {
        get { return m_Ball; }
    }

    public Vector3 InitialPosition
    {
        get { return m_InitialPosition; }
    }
    
    private void Awake()
    {
    }

    public void Die()
    {
        m_Ball.SetActive(false);

        m_RunnerMovement.SetMovementEnabled(false);
        m_PlayerInputMovement.SetInputEnabled(false);
    }

    public void ResetAll()
    {
        transform.position = m_InitialPosition;

        m_Ball.SetActive(true);

        m_RunnerMovement.SetMovementEnabled(true);
        m_PlayerInputMovement.SetInputEnabled(true);
        m_PlayerInputMovement.ResetAll();
    }
}
