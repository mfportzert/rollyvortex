﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarnedPointsCanvasSpawner : MonoBehaviour
{
    [SerializeField] private PointsEarnedCanvas m_PointsEarnedCanvasPrefab = null;
    [SerializeField] private Transform m_SpawnPoint = null;

    private void Awake()
    {
        Events.Gameplay.ScoreUpdated.Register(OnScoreUpdated);
    }

    private void OnDestroy()
    {
        Events.Gameplay.ScoreUpdated.Unregister(OnScoreUpdated);
    }

    private void OnScoreUpdated(int newScore, int scoreDelta)
    {
        if (scoreDelta > 0)
        {
            SpawnPointsEarnedWorldCanvas(scoreDelta);
        }
    }

    private void SpawnPointsEarnedWorldCanvas(int pointsEarned)
    {
        PointsEarnedCanvas pointsEarnedCanvasPrefab = Pools.GetFromPool(m_PointsEarnedCanvasPrefab);
        pointsEarnedCanvasPrefab.transform.position = m_SpawnPoint.position;

        pointsEarnedCanvasPrefab.SetEarnedPoints(pointsEarned);
    }
}
