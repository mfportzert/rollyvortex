﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeOnCollision : MonoBehaviour
{
    [SerializeField] private ParticleSystem m_ExplosionFXPrefab = null;
    
    private void Awake()
    {
        Events.Gameplay.ObstacleHit.Register(OnObstacleHit);
    }

    private void OnDestroy()
    {
        Events.Gameplay.ObstacleHit.Unregister(OnObstacleHit);
    }

    private void OnObstacleHit(Vector3 impactPoint)
    {
        ParticleSystem explosionFX = Pools.GetFromPool(m_ExplosionFXPrefab);
        explosionFX.transform.position = impactPoint;
    }
}
