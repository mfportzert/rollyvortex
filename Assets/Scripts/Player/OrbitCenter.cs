﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class OrbitCenter : MonoBehaviour
{
    [SerializeField] private GameObject m_OrbitTarget = null;

    [Header("Orbit config")]
    [SerializeField] private float m_OrbitRadius = 0;

    void Start()
    {
        
    }

    void Update()
    {
#if UNITY_EDITOR
        UpdateEditor();
#endif
    }

    private void UpdateEditor()
    {
        if (!Application.isPlaying)
        {
            if (m_OrbitTarget != null)
            {
                SetupBallInitialPosition();
            }
        }
    }

    private void SetupBallInitialPosition()
    {
        Vector3 targetPosition = Vector3.zero;
        targetPosition.y -= m_OrbitRadius;

        m_OrbitTarget.transform.localPosition = targetPosition;
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Vector3 position = transform.position;

        Gizmos.color = Color.green;
        Gizmos.DrawSphere(position, 0.05f);

        UnityEditor.Handles.color = Color.red;
        UnityEditor.Handles.DrawWireDisc(position, Vector3.forward, m_OrbitRadius);
    }
#endif
}
