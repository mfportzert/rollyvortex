﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunnerMovement : MonoBehaviour
{
    [SerializeField] private Vector3 m_MoveDirection = Vector3.forward;

    private bool m_MovementEnabled = true;

    private float m_RunTime = 0f;

    void Start()
    {
    }

    public void SetMovementEnabled(bool movementEnabled)
    {
        m_MovementEnabled = movementEnabled;

        if (!m_MovementEnabled)
        {
            m_RunTime = 0f;
        }
    }

    void Update()
    {
        if (m_MovementEnabled)
        {
            m_RunTime += Time.deltaTime;

            ApplyMovement();
        }
    }

    private void ApplyMovement()
    {
        GameConfigSO gameConfig = GameManager.Instance.GameConfig;

        float speed = gameConfig.BaseSpeed;

        float curveIndex = m_RunTime / gameConfig.SecondsToReachMaxAcceleration;
        float speedFactor = gameConfig.AccelerationCurve.Evaluate(curveIndex) * gameConfig.AccelerationFactor;
        speed += gameConfig.BaseSpeed * speedFactor;

        Vector3 moveDelta = speed * Time.deltaTime * m_MoveDirection;
        transform.position += moveDelta;
    }
}
