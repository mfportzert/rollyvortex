﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementInput : MonoBehaviour
{
    [SerializeField] private OrbitCenter m_OrbitCenter = null;
    [SerializeField] private float m_RotationSpeedEditor = 500f;
    [SerializeField] private float m_RotationSpeedMobile = 50f;

    private bool m_InputEnabled = true;

    private int m_FingerId = -1;
    private Vector2 m_TotalSwipeDelta; // Inches

    public void SetInputEnabled(bool inputEnabled)
    {
        m_InputEnabled = inputEnabled;
    }

    private void Update()
    {
        if (m_InputEnabled)
        {
#if UNITY_EDITOR
            UpdateEditor();
#elif UNITY_ANDROID || UNITY_IOS
            UpdateMobile();
#endif
        }
    }

    private void UpdateEditor()
    {
        float hAxis = Input.GetAxis("Horizontal");
        if (!Mathf.Approximately(hAxis, 0))
        {
            ApplyInputMovement(hAxis);
        }
    }

    private void UpdateMobile()
    {
        if (Input.touchCount == 0)
        {
            return;
        }

        int count = Input.touchCount;
        for (int i = 0; i < count; i++)
        {
            Touch touch = Input.GetTouch(i);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    if (m_FingerId == -1)
                    {
                        m_TotalSwipeDelta = Vector2.zero;
                        m_FingerId = touch.fingerId;
                    }
                    break;

                case TouchPhase.Moved:
                    if (touch.fingerId == m_FingerId)
                    {
                        ApplyTouchMovement(touch.deltaPosition * Time.deltaTime / touch.deltaTime);
                    }
                    break;

                case TouchPhase.Ended:
                    if (touch.fingerId == m_FingerId)
                    {
                        m_FingerId = -1;
                    }
                    break;
            }
        }
    }

    private void ApplyTouchMovement(Vector2 touchDeltaPosition)
    {
        Vector2 physicalDistance = touchDeltaPosition / Screen.dpi;

        float angleDelta = physicalDistance.x * m_RotationSpeedMobile;

        m_OrbitCenter.transform.Rotate(Vector3.forward, angleDelta);
    }

    private void ApplyInputMovement(float hAxis)
    {
        float angleDelta = hAxis * m_RotationSpeedEditor * Time.deltaTime;

        m_OrbitCenter.transform.Rotate(Vector3.forward, angleDelta);
    }

    public void ResetAll()
    {
        m_OrbitCenter.transform.rotation = Quaternion.identity;
    }
}
