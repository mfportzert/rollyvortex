﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    [SerializeField] private Vector3 m_RotationAxis = Vector3.zero;
    [SerializeField] private float m_RotationSpeed = 10f;

    void Update()
    {
        ApplyRotation();
    }

    private void ApplyRotation()
    {
        transform.Rotate(m_RotationAxis, m_RotationSpeed * Time.deltaTime);
    }
}
