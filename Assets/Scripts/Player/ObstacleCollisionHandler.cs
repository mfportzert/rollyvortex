﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleCollisionHandler : MonoBehaviour
{
    [SerializeField] private LayerMask m_ObstacleLayerMask;

    private void OnTriggerEnter(Collider other)
    {
        if (m_ObstacleLayerMask.IsInMask(other.gameObject.layer))
        {
            Vector3 impactPoint = other.GetComponent<Collider>().ClosestPointOnBounds(transform.position);
            Events.Gameplay.ObstacleHit.Broadcast(impactPoint);
        }
    }
}
