﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ObstacleGroupConfig
{
    public Obstacle ObstaclePrefab = null;
    public int MinGroupAmount = 2;
    public int MaxGroupAmount = 6;
}

public class ObstaclesConfigSO : ScriptableObject
{
    [SerializeField] private List<ObstacleGroupConfig> m_ObstacleGroups = null;

    public List<ObstacleGroupConfig> ObstacleGroups { get { return m_ObstacleGroups; } }

    public ObstacleGroupConfig GetRandomObstacleGroup()
    {
        return m_ObstacleGroups[UnityEngine.Random.Range(0, m_ObstacleGroups.Count)];
    }
}