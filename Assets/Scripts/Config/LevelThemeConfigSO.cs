﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class LevelThemeConfigSO : ScriptableObject
{
    [SerializeField] private List<Color> m_ObstacleColors = null;

    public List<Color> ObstacleColors { get { return m_ObstacleColors; } }

    public Color GetRandomObstacleColor()
    {
        return m_ObstacleColors[UnityEngine.Random.Range(0, m_ObstacleColors.Count)];
    }
}