﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ChallengeConfig
{
    [SerializeField] private string m_ChallengeName = "Fast 1";
    [SerializeField] private string m_Description = "";
    [SerializeField] private Sprite m_Icon = null;
    [SerializeField] private GameConfigSO m_GameConfig = null;

    public string Name { get { return m_ChallengeName; } }
    public string Description { get { return m_Description; } }
    public Sprite Icon { get { return m_Icon; } }
    public GameConfigSO GameConfig { get { return m_GameConfig; } }
}

[Serializable]
public class ChallengesSectionConfig
{
    [SerializeField] private string m_SectionName = "Bronze";
    [SerializeField] private List<ChallengeConfig> m_Challenges = null;

    public string SectionName { get { return m_SectionName; } }
    public List<ChallengeConfig> Challenges { get { return m_Challenges; } }
}

public class ChallengesConfigSO : ScriptableObject
{
    [SerializeField] private List<ChallengesSectionConfig> m_Sections = null;

    public List<ChallengesSectionConfig> Sections { get { return m_Sections; } }
}