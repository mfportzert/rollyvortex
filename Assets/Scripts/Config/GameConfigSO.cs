﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameConfigSO : ScriptableObject
{
    [Header("Score")]
    [SerializeField] private int m_ScorePerObstacle = 0;

    public int ScorePerObstacle { get { return m_ScorePerObstacle; } }

    [Header("Movement Speed")]
    [SerializeField] private float m_BaseSpeed = 5f;
    [SerializeField] private AnimationCurve m_AccelerationCurve = null;
    [SerializeField] private float m_AccelerationFactor = 1.5f;
    [SerializeField] private float m_SecondsToReachMaxAcceleration = 100f;

    public float BaseSpeed { get { return m_BaseSpeed; } }
    public AnimationCurve AccelerationCurve { get { return m_AccelerationCurve; } }
    public float AccelerationFactor { get { return m_AccelerationFactor; } }
    public float SecondsToReachMaxAcceleration { get { return m_SecondsToReachMaxAcceleration; } }

    [Header("Theme")]
    [SerializeField] private LevelThemeConfigSO m_ThemeConfig = null;

    public LevelThemeConfigSO ThemeConfig { get { return m_ThemeConfig; } }

    [Header("Obstacles")]
    [SerializeField] private float m_ObstaclesDistance = 4f;
    [SerializeField] private ObstaclesConfigSO m_ObstaclesConfig = null;

    public float ObstaclesDistance { get { return m_ObstaclesDistance; } }
    public ObstaclesConfigSO ObstaclesConfig { get { return m_ObstaclesConfig; } }
}
