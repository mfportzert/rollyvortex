﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class MedalConfig
{
    [SerializeField] private int m_UnlockScore = 50;
    [SerializeField] private string m_MedalName = "Bronze";
    [SerializeField] private string m_Description = "";
    [SerializeField] private Sprite m_Icon = null;

    public int UnlockScore { get { return m_UnlockScore; } }
    public string Name { get { return m_MedalName; } }
    public string Description { get { return m_Description; } }
    public Sprite Icon { get { return m_Icon; } }
}

public class MedalsConfigSO : ScriptableObject
{
    [SerializeField] private List<MedalConfig> m_Medals = null;

    public List<MedalConfig> Medals { get { return m_Medals; } }
}