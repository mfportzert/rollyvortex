﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ScoreController : MonoBehaviour
{
    private int m_PlayerScore = 0;
    private int m_PlayerHighScore = 0;
    private int m_PreviousPlayerHighScore = 0;

    public int PlayerScore
    {
        get { return m_PlayerScore; }
    }

    public int PlayerHighScore
    {
        get { return m_PlayerHighScore; }
    }

    public int PreviousPlayerHighScore
    {
        get { return m_PreviousPlayerHighScore; }
    }

    private void Awake()
    {
        Events.Gameplay.ObstaclePassed.Register(OnObstaclePassed);

        Events.Save.SaveGame.Register(OnSaveGame);
        Events.Save.LoadGame.Register(OnLoadGame);
    }

    private void OnDestroy()
    {
        Events.Gameplay.ObstaclePassed.Unregister(OnObstaclePassed);

        Events.Save.SaveGame.Unregister(OnSaveGame);
        Events.Save.LoadGame.Unregister(OnLoadGame);
    }

    public bool HasNewHighScore()
    {
        return m_PlayerHighScore > m_PreviousPlayerHighScore;
    }

    public void ResetScore()
    {
        m_PreviousPlayerHighScore = m_PlayerHighScore;

        SetPlayerScore(0);
    }

    private void SetPlayerScore(int score)
    {
        int scoreDelta = score - m_PlayerScore;

        m_PlayerScore = score;
        Events.Gameplay.ScoreUpdated.Broadcast(score, scoreDelta);

        if (m_PlayerScore > m_PlayerHighScore)
        {
            m_PlayerHighScore = m_PlayerScore;
            Events.Gameplay.HighScoreUpdated.Broadcast(m_PlayerHighScore);
        }
    }

    private void AddPlayerScore(int points)
    {
        SetPlayerScore(m_PlayerScore + points);
    }

    private void OnObstaclePassed()
    {
        AddPlayerScore(GameManager.Instance.GameConfig.ScorePerObstacle);
    }

    private void OnSaveGame(SaveState saveState)
    {
        saveState.HighScore = m_PlayerHighScore;
        saveState.PreviousHighScore = m_PreviousPlayerHighScore;
    }

    private void OnLoadGame(SaveState saveState)
    {
        m_PlayerHighScore = saveState.HighScore;
        m_PreviousPlayerHighScore = saveState.PreviousHighScore;
    }
}
