﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReturnToPoolOnFxEnd : MonoBehaviour
{
    [SerializeField] private ParticleSystem m_ParticleSystem = null;

    private void Update()
    {
        if (!m_ParticleSystem.IsAlive())
        {
            Pools.Send(gameObject);
        }
    }
}
