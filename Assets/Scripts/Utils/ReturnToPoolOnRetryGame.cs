﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReturnToPoolOnRetryGame : MonoBehaviour
{
    private void Awake()
    {
        Events.Gameplay.RetryGame.Register(OnRetryGame);
    }

    private void OnDestroy()
    {
        Events.Gameplay.RetryGame.Unregister(OnRetryGame);
    }

    private void OnRetryGame()
    {
        Pools.Send(gameObject);
    }
}
