﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
    protected static T m_Instance;

    [SerializeField] private bool m_DontDestroy = false;

    protected virtual void Awake()
    {
        if (m_DontDestroy)
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    public static T Instance
    {
        get
        {
#if UNITY_EDITOR
            if (s_ApplicationIsQuitting)
            {
                //Debug.LogWarning("[Singleton] Instance '" + typeof(T) +
                //    "' already destroyed on application quit." +
                //    " Won't create again - returning null.");
                return null;
            }
#endif

            if (m_Instance == null)
            {
                m_Instance = (T) FindObjectOfType(typeof(T)) as T;

                if (m_Instance == null)
                {
                    GameObject singletonObj = new GameObject("_Singleton_<" + typeof(T).ToString() + ">");
                    m_Instance = singletonObj.AddComponent<T>();
                }
            }
            return m_Instance;
        }
    }

#if UNITY_EDITOR
    private static bool s_ApplicationIsQuitting = false;

    /// <summary>
    /// When Unity quits, it destroys objects in a random order.
    /// In principle, a Singleton is only destroyed when application quits.
    /// If any script calls Instance after it have been destroyed, 
    ///   it will create a buggy ghost object that will stay on the Editor scene
    ///   even after stopping playing the Application. Really bad!
    /// So, this was made to be sure we're not creating that buggy ghost object.
    /// </summary>
    public void OnApplicationQuit()
    {
        s_ApplicationIsQuitting = true;
    }
#endif
}