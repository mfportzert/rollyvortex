﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AnimParams : MonoBehaviour
{
    public static int ANIM_TRIGGER_AddScore = Animator.StringToHash("AddScore");
    public static int ANIM_TRIGGER_CameraFlash = Animator.StringToHash("CameraFlash");
    public static int ANIM_TRIGGER_ScreenOpen = Animator.StringToHash("ScreenOpen");
    public static int ANIM_TRIGGER_ScreenResult = Animator.StringToHash("ScreenResult");
    public static int ANIM_TRIGGER_ScreenResultNewBest = Animator.StringToHash("ScreenResultNewBest");
    public static int ANIM_TRIGGER_ScreenReset = Animator.StringToHash("ScreenReset");
    public static int ANIM_TRIGGER_UnlockMedal = Animator.StringToHash("UnlockMedal");
    public static int ANIM_TRIGGER_ResetUnlockMedal = Animator.StringToHash("ResetUnlockMedal");

    public static int ANIM_BOOL_IsHighScoreMode = Animator.StringToHash("isHighScoreMode");
    public static int ANIM_BOOL_GatePassed = Animator.StringToHash("gatePassed");
}
