﻿using UnityEngine;

public static class LayerMaskExtensions
{
    public static bool IsInMask(this LayerMask layerMask, int layer)
    {
        return (layerMask == (layerMask | (1 << layer)));
    }

    public static int GetLayerValue(this LayerMask layerMask)
    {
        return (int) (Mathf.Log(layerMask.value) / Mathf.Log(2));
    }
}
