﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    [SerializeField] private Transform m_CamTransform = null;

    // How long the object should shake for.
    [SerializeField] private float m_DefaultShakeDuration = 0.5f;

    // Amplitude of the shake. A larger value shakes the camera harder.
    [SerializeField] private float m_ShakeAmount = 0.7f;
    [SerializeField] private float m_DecreaseFactor = 1.0f;

    Vector3 originalPos;
    private float m_ShakeRemainingDuration = 0f;

    private void Awake()
    {
        Events.Camera.Shake.Register(OnCameraShake);
    }

    private void OnDestroy()
    {
        Events.Camera.Shake.Unregister(OnCameraShake);
    }

    private void OnCameraShake()
    {
        Shake(m_DefaultShakeDuration);
    }

    public void Shake(float duration)
    {
        m_ShakeRemainingDuration = duration;
    }

    void OnEnable()
    {
        originalPos = m_CamTransform.localPosition;
    }

    void Update()
    {
        if (m_ShakeRemainingDuration > 0)
        {
            m_CamTransform.localPosition = originalPos + Random.insideUnitSphere * m_ShakeAmount;

            m_ShakeRemainingDuration -= Time.deltaTime * m_DecreaseFactor;
        }
        else
        {
            m_ShakeRemainingDuration = 0f;
            m_CamTransform.localPosition = originalPos;
        }
    }
}
