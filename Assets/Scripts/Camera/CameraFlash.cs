﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFlash : MonoBehaviour
{
    [SerializeField] private Animator m_Animator = null;

    private void Awake()
    {
        Events.Camera.Flash.Register(OnCameraFlash);
    }

    private void OnDestroy()
    {
        Events.Camera.Flash.Unregister(OnCameraFlash);
    }

    private void OnCameraFlash()
    {
        m_Animator.SetTrigger(AnimParams.ANIM_TRIGGER_CameraFlash);
    }
}
