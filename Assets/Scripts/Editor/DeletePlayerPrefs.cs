﻿using UnityEngine;
using UnityEditor;

public class DeletePlayerPrefsScript : EditorWindow
{
    [MenuItem("Tools/Delete PlayerPrefs (All)")]
    static void DeleteAllPlayerPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
}