﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pools
{
	private static Pools instance = new Pools();

	public static Pool GetPool(GameObject prefabObject, bool createPoolIfNotExist = true)
	{
		Pool pool = instance.FindRegisteredPool (prefabObject);
        if (pool == null && createPoolIfNotExist)
        {
            Debug.Log("Creating pool for prefab: '" + prefabObject.name + "'.");
            pool = CreatePool(prefabObject);
        }

        return pool;
	}
    
    private static Pool GetOrCreatePool(GameObject prefabObject, bool createPoolIfNotExist = true)
    {
        Pool pool = GetPool(prefabObject);
        if (pool == null)
        {
            if (createPoolIfNotExist)
            {
                Debug.Log("Creating pool for prefab: '" + prefabObject.name + "'.");
                pool = CreatePool(prefabObject);
            }
            else
            {
                Debug.LogWarning("Registered pool not found for prefab: '" + prefabObject.name + "'.");
            }
        }

        return pool;
    }

	public static GameObject GetFromPool(GameObject prefabObject, bool createPoolIfNotExist = true)
	{
        Pool pool = GetOrCreatePool(prefabObject, createPoolIfNotExist);

        GameObject instanceObj = (pool != null) ? pool.GetObject() : null;
        return instanceObj;
	}


    public static T GetFromPool<T>(T prefabObject, bool createPoolIfNotExist = true) where T : Component
    {
        GameObject instanceObj = GetFromPool(prefabObject.gameObject);

        return (instanceObj != null) ? instanceObj.GetComponent<T>() : null;
    }

    public static bool Send(GameObject instanceObj, bool destroyIfUnableToSend = true)
	{
		bool success = false;

		Pooled pooled = instanceObj.GetComponent<Pooled>();
		if (pooled != null)
		{
            pooled.SendBackToPool();
			success = true;
		}
		else
		{
			Debug.LogWarning("Object '"+instanceObj.name+"' cannot be recycled because it doesn't belong to a pool.");
			if (destroyIfUnableToSend)
			{
				GameObject.Destroy (instanceObj);
			}
		}

		return success;
	}

	public static void RegisterPool(Pool pool)
	{
		instance.registeredPools[pool.Prefab.GetInstanceID()] = pool;
	}

	public static void UnregisterPool(Pool pool)
	{
		instance.registeredPools.Remove(pool.Prefab.GetInstanceID());
	}

	public static Pool CreatePool(GameObject prefabObject, int maxCapacity = 100, int preloadAmount = 1)
	{
        if (prefabObject == null)
        {
            return null;
        }

		Pool pool = new GameObject(prefabObject.name + "Pool").AddComponent<Pool>();
		pool.Prefab = prefabObject;
		pool.MaxCapacity = maxCapacity;

		pool.Init (preloadAmount);

		return pool;
	}

	private IDictionary<int, Pool> registeredPools = new Dictionary<int, Pool>();

	private Pools()
	{
	}

	private Pool FindRegisteredPool(GameObject prefabObject)
	{
        if (prefabObject == null)
        {
            return null;
        }

		Pool pool;
		if (!registeredPools.TryGetValue(prefabObject.GetInstanceID(), out pool))
		{
			Debug.LogWarning("No registered pool for prefab '"+prefabObject.name+"'");
		}

		return pool;
	}
}
