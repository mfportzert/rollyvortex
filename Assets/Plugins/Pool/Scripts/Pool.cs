﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class Pool : MonoBehaviour
{
	public enum PoolPolicy
	{
		FirstAvailable,
		RoundRobin
	}
	
	[SerializeField] private GameObject prefab = null;
	[SerializeField] private PoolPolicy policy = PoolPolicy.FirstAvailable;
	[SerializeField] private int maxCapacity = 100;
	[SerializeField] private int preloadAmount = 0;

	public UnityEvent onMaxCapacityReached;

	// Used by custom inspector to show real-time pool state in play mode
	[SerializeField] private List<Pooled> unavailableObjects = new List<Pooled>();
	[SerializeField] private List<Pooled> availableObjects = new List<Pooled>();

	private int availableObjectsIndex = 0;

	private bool registered = false;

	public int MaxCapacity
	{
		get 
		{
			return maxCapacity;
		}
		set
		{
			if (value < CurrentCapacity)
			{
				Debug.LogWarning("Cannot reduce MaxCapacity under current capacity ("+CurrentCapacity+").");
			}
			else
			{
				maxCapacity = value;
			}
		}
	}

	public int CurrentCapacity
	{
		get
		{
			return unavailableObjects.Count + availableObjects.Count;
		}
	}

	public PoolPolicy Policy
	{
		get 
		{
			return policy;
		}
		set
		{
			policy = value;
		}
	}

	public GameObject Prefab
	{
		get 
		{
			return prefab;
		}
		set
		{
			if (!registered)
			{
				prefab = value;
                Init(preloadAmount);
            }
			else
			{
				Debug.LogWarning("Cannot change prefab while pool is active.");
			}
		}
	}

	void Awake()
	{
		if (!registered)
		{
			Init (preloadAmount);
		}
	}

	public void Init(int preloadAmount = 0)
	{
        if (prefab != null)
        {
            Pools.RegisterPool(this);
            registered = true;

            Load(preloadAmount);
        }
    }

	public void Reset()
	{
		Pools.UnregisterPool (this);
		registered = false;
		
		availableObjects.Clear ();
		unavailableObjects.Clear ();
	}

	public GameObject GetObject()
	{
		Pooled pooledObj = GetFromAvailableObjects();

		if (pooledObj != null)
		{
			availableObjects.RemoveAt(availableObjectsIndex);
		}
		else if (CurrentCapacity < maxCapacity || maxCapacity == 0)
		{
			pooledObj = CreateNewObject();
		}

		if (pooledObj != null)
		{
			pooledObj.gameObject.SetActive (true);
			unavailableObjects.Add(pooledObj);

            pooledObj.NotifyExitPool();

            return pooledObj.gameObject;
        }

        return null;
	}

	private Pooled CreateNewObject()
	{
        Pooled pooled = null;

		if (prefab != null)
		{
			GameObject pooledObj = Instantiate(prefab);
			pooledObj.transform.SetParent(transform, false);
            pooledObj.SetActive(false);

            pooled = pooledObj.AddComponent<Pooled>();
            pooled.OwnerPool = this;
        }
		
		if (MaxCapacity > 0 && CurrentCapacity + 1 == MaxCapacity)
		{
			Debug.LogWarning("Max capacity reached in Pool '"+gameObject.name+"'");
			onMaxCapacityReached.Invoke();
		}

		return pooled;
	}

	private Pooled GetFromAvailableObjects()
	{
        Pooled pooledObj = null;

		while (pooledObj == null && availableObjects.Count > 0)
		{
			UpdateIndexByPolicy();

			pooledObj = availableObjects[availableObjectsIndex];
			if (pooledObj == null)
			{
				Debug.LogWarning ("An object from Pool '"+gameObject.name+"' has been externally destroyed.");
				availableObjects.RemoveAt(availableObjectsIndex);
				availableObjectsIndex = Mathf.Max (availableObjectsIndex - 1, 0);
			}
			else
			{
				break;
			}
		}

		return pooledObj;
	}

	private void UpdateIndexByPolicy()
	{
		switch (policy)
		{
		case PoolPolicy.FirstAvailable:
			availableObjectsIndex = 0;
			break;

		case PoolPolicy.RoundRobin:
			availableObjectsIndex++;
			if (availableObjectsIndex >= availableObjects.Count)
			{
				availableObjectsIndex = 0;
			}
			break;
		}
	}

	public bool PutObject(GameObject gameObject)
	{
		return PutObject(gameObject.GetComponent<Pooled>());
	}

	public bool PutObject(Pooled pooled)
	{
		if (pooled.OwnerPool == this)
		{
			GameObject obj = pooled.gameObject;
            Transform objTransform = obj.transform;
            objTransform.SetParent(transform, false);
			obj.SetActive (false);

            pooled.NotifyEnterPool();

            availableObjects.Add(pooled);
			unavailableObjects.Remove(pooled);

			return true;
		}
		return false;
	}
	
	public void Load(int amount)
	{
		if (amount <= 0)
		{
			return;
		}

		int currentCapacity = CurrentCapacity;
		int amountToLoad = Mathf.Min (currentCapacity + amount, MaxCapacity) - currentCapacity;
		for (int i = 0; i < amountToLoad; i++)
		{
			Pooled pooled = CreateNewObject();
			availableObjects.Add(pooled);
		}
	}

	void OnDestroy()
	{
		Reset();
	}
}
