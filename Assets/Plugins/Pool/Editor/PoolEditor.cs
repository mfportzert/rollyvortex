﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEditor;

[CustomEditor(typeof(Pool))]
public class PoolEditor : Editor
{
	private SerializedProperty preloadAmountProp;
	private SerializedProperty maxCapacityProp;
	private SerializedProperty policyProp;
	private SerializedProperty prefabProp;
	private SerializedProperty onMaxCapacityReachedProp;

	private SerializedProperty availableListProp;
	private SerializedProperty unavailableListProp;

	void OnEnable()
	{
		preloadAmountProp = serializedObject.FindProperty ("preloadAmount");
		maxCapacityProp = serializedObject.FindProperty ("maxCapacity");
		policyProp = serializedObject.FindProperty ("policy");
		prefabProp = serializedObject.FindProperty ("prefab");
		
		availableListProp = serializedObject.FindProperty ("availableObjects");
		unavailableListProp = serializedObject.FindProperty ("unavailableObjects");
	}

	public override void OnInspectorGUI()
	{
		serializedObject.Update ();

		EditorGUILayout.PropertyField (prefabProp, new GUIContent ("Prefab"));
		EditorGUILayout.PropertyField (policyProp, new GUIContent ("Policy"));

		int maxCapacity = maxCapacityProp.intValue;
		if (EditorApplication.isPlayingOrWillChangePlaymode && EditorApplication.isPlaying)
		{
			int availableAmount = availableListProp.arraySize;
			int unavailableAmount = unavailableListProp.arraySize;
			int currentCapacity = availableAmount + unavailableAmount;

			float progress = (maxCapacity == 0) ? 1 : ((float) currentCapacity / (float) maxCapacity);
			string progressLabel = currentCapacity + " / " + ((maxCapacity == 0) ? "unlimited" : ""+maxCapacity) + " ("+availableAmount+" available)";

			EditorGUILayout.Space ();
			ProgressBar (progress, progressLabel);
		}
		else
		{
			EditorGUILayout.PropertyField (maxCapacityProp, new GUIContent ("Max capacity"));
			
			if (maxCapacity == 0)
			{
				EditorGUILayout.PropertyField (preloadAmountProp, new GUIContent ("Preload amount"));
			}
			else
			{
				EditorGUILayout.IntSlider (preloadAmountProp, 0, maxCapacity, new GUIContent ("Preload amount"));
			}
		}

		serializedObject.ApplyModifiedProperties ();
	}

	void ProgressBar (float value, string label)
	{
		Rect rect = GUILayoutUtility.GetRect (18, 18, "TextField");
		EditorGUI.ProgressBar (rect, value, label);
        EditorGUILayout.Space ();
	}
}
